package com.springapp.helpers;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Created by Edvin Mulabdic
 */
public class UserAccessHelper {

//    public static final String GUEST = "0";
//    public static final String ADMIN = "1";
//    public static final String USER = "2";

    public String adminAccess() {
        return "admin";
//                BCrypt.hashpw("adminUserAccess", BCrypt.gensalt());
    }

    public String guestAccess() {
        return BCrypt.hashpw("guest", BCrypt.gensalt());
    }

    public String userAccess() {
        return BCrypt.hashpw("userAccess", BCrypt.gensalt());
    }
}
