package com.springapp.service;

import java.sql.SQLException;

/**
 * Created by Edvin Mulabdic
 */
public interface AppUserService {
     String authenticate(String email,String password) throws SQLException;
}
