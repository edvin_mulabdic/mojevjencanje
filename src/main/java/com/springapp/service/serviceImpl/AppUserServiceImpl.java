package com.springapp.service.serviceImpl;

import com.springapp.dao.AppUserDao;
import com.springapp.dao.daoImpl.AppUserDaoImpl;
import com.springapp.model.AppUser;
import com.springapp.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/**
 * Created by Edvin Mulabdic
 */
@Service
public class AppUserServiceImpl implements AppUserService {
    @Autowired
    AppUserDao userDao;

    /* ------------------- authenticate user ------------------ */
    public String authenticate(String email, String password) throws SQLException {
//        userDaoImpl.getUserAccessRole();
        AppUser appUser = userDao.findUserByEmail("edvin@gmail.com");

       return (appUser != null && BCrypt.checkpw(password, appUser.getPassword())) ? "admin" : null;
    }

}
