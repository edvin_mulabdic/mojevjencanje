package com.springapp.dao;

import com.springapp.model.AppUser;
import com.springapp.model.Role;

import java.sql.SQLException;

/**
 * Created by ajla.eltabari on 15.5.2017.
 */
public interface AppUserDao {
    AppUser findUserByEmail(String email) throws SQLException;
}
