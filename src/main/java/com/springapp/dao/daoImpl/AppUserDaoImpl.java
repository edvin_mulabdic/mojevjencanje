package com.springapp.dao.daoImpl;

import com.springapp.dao.AbstractDao;
import com.springapp.dao.AppUserDao;
import com.springapp.model.AppUser;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by Edvin Mulabdic
 */
@Repository
@Transactional
public class AppUserDaoImpl extends AbstractDao<Integer, AppUser> implements AppUserDao  {

    public AppUser findUserByEmail(String email) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("email", email));
        return (AppUser) criteria.uniqueResult();
    }
}
