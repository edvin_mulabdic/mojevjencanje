package com.springapp.controller;

import com.springapp.helpers.UserAccessHelper;
import com.springapp.service.AppUserService;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;


/**
 * Created by Edvin Mulabdic
 */
@Controller
public class LoginController {
    @Autowired
    AppUserService appUserService;

    @Autowired
    UserAccessHelper userAccessHelper;

    @RequestMapping(value= "/login", method = RequestMethod.GET)
    public String getLoginPage() {
        return "login";
    }

    @RequestMapping(value= "/login_access", method = RequestMethod.POST)
    public String adminLogin(HttpServletRequest request, Model model) throws SQLException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if (appUserService.authenticate(email, password).equals(userAccessHelper.adminAccess())) {
            return "/admin/admin_panel";
        } else if (appUserService.authenticate(email, password).equals(userAccessHelper.userAccess())) {
            return "homepage";
        } else {
            model.addAttribute("message", "Netacan username ili password");
            return "login";
        }
    }
}
