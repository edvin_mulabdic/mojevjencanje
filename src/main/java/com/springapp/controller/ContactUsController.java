package com.springapp.controller;

import com.springapp.helpers.MailHelpers;
import com.springapp.helpers.MessageHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Edvin Mulabdic
 */
@Controller
public class ContactUsController extends HttpServlet {
    @Autowired
    private JavaMailSender mailSender;

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @RequestMapping(value= "/contact_us", method = RequestMethod.GET)
    public ModelAndView getContactUsPage() {
        return new ModelAndView("contact_us", "command", null);
    }

    @RequestMapping(value= "/create_account", method = RequestMethod.POST)
    public String sendMail(HttpServletRequest request, Model model) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            message.setSubject(MailHelpers.MAIL_SUBJECT);
            MimeMessageHelper helper;
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(MailHelpers.MAIL_FROM);
            helper.setTo(MailHelpers.MAIL_TO);
            helper.setText(request.getParameter("email") + "\n" +
                    request.getParameter("firstName") + "\n" +
                    request.getParameter("lastName") + "\n" +
                    request.getParameter("phone") + "\n" +
                    request.getParameter("comment"), true);
            mailSender.send(message);
            model.addAttribute("message", MessageHelpers.MAIL_SUCCESS_MSG);
            return "login";
        } catch (MessagingException ex) {
            Logger.getLogger(ContactUsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        model.addAttribute("message", MessageHelpers.MAIL_ERROR_MSG);
        return "contact_us";
    }

}
