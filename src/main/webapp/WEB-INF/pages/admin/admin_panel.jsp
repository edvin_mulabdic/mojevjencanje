<%--
  Created by Edvin Mulabdic
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix ="form" uri = "http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:wrapper>
    <!-- Main Container -->
    <section class="main-container col2-right-layout wow bounceInUp animated">
        <div class="main container">
            <div class="row">
                <div class="col-main col-sm-9">
                    <div class="my-account">
                        <div class="page-title">
                            <h2>Admin Panel</h2>
                            <br>
                        </div>
                        <div class="dashboard">
                            <div class="box-account">
                                <div class="page-title">
                                    <h2>Informacije o profilu</h2>
                                </div>
                                <div class="col2-set">
                                    <div class="col-1">
                                        <form  method="GET" role="form" action="@routes.AppUsers." >
                                            <h5>Kontakt Informacije</h5>
                                            <p> Ime: <br>
                                                Adresa: <br>
                                                Grad: <br>
                                                Telefon:
                                                <br>
                                                <br>
                                            </p>
                                            <div class="buttons-set">
                                                <button id="send2" name="send" type="submit" class="button login"><span>Ažuriraj</span></button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-2">
                                        <form  method="POST" role="form" action="@routes.AppUsers.updateAdminPassword(user.id)" >
                                            <h5>Pristupne informacije</h5>
                                            <p></p>
                                            <input type="password" title="Password" class="input-text" name="password" required>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="buttons-set">
                                                <button id="send2" name="send" type="submit" class="button login"><span>Ažuriraj</span></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <aside class="col-right sidebar col-sm-3">
                    <div class="block block-account">
                        <div class="block-title">Upravljanje stranicom</div>
                        <div class="block-content">
                            <ul>
                                <li class="current"><a>Admin Panel</a></li>
                                <li><a href="">Vjenčanice</a></li>
                                <li><a href="">Odijela</a></li>
                                <li><a href="">Cvjećare</a></li>
                                <li><a href="">Zlatare</a></li>
                                <li><a href="">Sale</a></li>
                                <li><a href="">Dodaj korisnika</a></li>
                                <li><a href="">Proizvodi</a></li>
                                <li><a href="">Kategorije</a></li>
                                <li><a href="">Dodaj kategoriju</a></li>
                                <li><a href="">Blog</a></li>
                                <li><a href="">Kreiraj blog</a></li>

                            </ul>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>

</t:wrapper>
