<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix ="form" uri = "http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:wrapper>
    <!-- Breadcrumbs -->
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <ul>
                    <li class="home"> <a title="Početna" href="/">Početna</a><span>&mdash;›</span></li>
                    <li class="category13"><strong>Kontaktirajte nas</strong></li>
                </ul>
            </div>
        </div>
    </section>
    <!-- Breadcrumbs End -->
    <!-- Main Container -->
    <section class="main-container col2-right-layout wow bounceInUp animated">
        <div class="main container">
            <div class="row">
                <c:if test="${not empty message}">
                    <div class="our-features-box wow bounceInUp animated">
                        <div class="feature-box">
                            <div class="content">
                                <c:out value="${message}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <section class="col-main col-sm-9">
                    <div class="page-title">
                        <h2>Kontaktirajte nas</h2>
                    </div>
                    <div class="static-contain">
                        <fieldset class="group-select">
                            <ul>
                                <li id="billing-new-address-form">
                                    <form:form method="POST" action="/create_account">
                                        <fieldset>
                                            <ul>
                                                <li>
                                                    <div class="customer-name">
                                                        <div class="input-box name-firstname">
                                                            <label for="billing:firstname"> Ime <span class="required">*</span></label>
                                                            <br>
                                                            <input type="text" id="firstname" name="firstName" title="Ime" class="input-text " required>
                                                        </div>
                                                        <div class="input-box name-lastname">
                                                            <label for="lastname"> Prezime <span class="required">*</span> </label>
                                                            <br>
                                                            <input type="text" id="lastname" name="lastName" title="Prezime" class="input-text" required>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="input-box">
                                                        <label for="email">Email</label>
                                                        <br>
                                                        <input type="text" id="email" name="email" title="Email" class="input-text" required>
                                                    </div>
                                                    <div class="input-box">
                                                        <label for="phone">Telefon <span class="required">*</span></label>
                                                        <br>
                                                        <input type="text" name="phone" id="phone" title="Telefon" class="input-text" required>
                                                    </div>
                                                </li>

                                                <li class="">
                                                    <label for="comment">Poruka<em class="required">*</em></label>
                                                    <br>
                                                    <div class="">
                                                        <textarea name="comment" id="comment" title="Poruka" class="input-text" cols="5" rows="3" required></textarea>
                                                    </div>
                                                </li>
                                            </ul>
                                        </fieldset>
                                        <li><span class="require"><em class="required">* </em>Required Fields</span></li>
                                        <li>
                                            <div class="buttons-set">
                                                <button type="submit" title="Submit" class="button submit"> <span> Submit </span> </button>
                                            </div>
                                        </li>
                                    </form:form>
                                </li>
                            </ul>

                        </fieldset>
                    </div>
                </section>
                <aside class="col-right sidebar col-sm-3">
                    <div class="block block-company">
                        <div class="block-title">Company </div>
                        <div class="block-content">
                            <ol id="recently-viewed-items">
                                <li class="item odd"><a href="about_us.html">About Us</a></li>
                                <li class="item even"><a href="sitemap.html">Sitemap</a></li>
                                <li class="item  odd"><a href="#">Terms of Service</a></li>
                                <li class="item even"><a href="#">Search Terms</a></li>
                                <li class="item last"><strong>Contact Us</strong></li>
                            </ol>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
    <!-- Main Container End -->

    <!-- service -->
    <div class="our-features-box wow bounceInUp animated">
        <ul>
            <li>
                <div class="feature-box">
                    <div class="icon-truck"></div>
                    <div class="content">FREE SHIPPING on order over $99</div>
                </div>
            </li>
            <li>
                <div class="feature-box">
                    <div class="icon-support"></div>
                    <div class="content">Need Help +1 800 123 1234</div>
                </div>
            </li>
            <li>
                <div class="feature-box">
                    <div class="icon-money"></div>
                    <div class="content">Money Back Guarantee</div>
                </div>
            </li>
            <li class="last">
                <div class="feature-box">
                    <div class="icon-return"></div>
                    <div class="content">30 days return Service</div>
                </div>
            </li>
        </ul>
    </div>
    <!-- end service -->
</t:wrapper>