<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix ="form" uri = "http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:wrapper>
        <section class="main-container col1-layout bounceInUp animated">
            <div class="main container">
                <div class="account-login">
                    <div class="page-title">
                        <h2>Prijavite se ili kreirajte profil</h2>
                    </div>
                    <c:if test="${not empty message}">
                        <div class="message-box wow bounceInDown animated">
                            <div class="message-text">
                                <c:out value="${message}"/>
                            </div>
                        </div>
                    </c:if>
                    <fieldset class="col2-set">
                        <div class="col-1 new-users"><strong>Novi korisnici</strong>
                            <div class="content">
                                <p>Ukoliko želite kreirati profil molimo Vas da nas kontaktirate putem email-a.
                                    Kreiranjem profila biće Vam omogućeno da prezentujete svoje artikle, dodate radnje na mapu, pratite kupovinu,
                                    i još mnoge druge pogodnosti.</p>
                                <div class="buttons-set">
                                    <a href="contact_us" class="button"><i class="fa fa-envelope"></i><span>Kontaktirajte nas</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 registered-users"><strong>Registrovani korisnici</strong>
                            <div class="content">
                                <p>Ukoliko imate profil, molimo Vas da unesete podatke.</p>
                                <form:form method="POST" action="/login_access">
                                    <ul class="form-list">
                                        <li>
                                            <label for="email">Email Adresa <span class="required">*</span></label>
                                            <br>
                                            <input type="text" title="Email Adresa" class="input-text" id="email" value="" name="email">
                                        </li>
                                        <li>
                                            <label for="pass">Password <span class="required">*</span></label>
                                            <br>
                                            <input type="password" title="Password" id="pass" class="input-text" name="password">
                                        </li>
                                    </ul>
                                    <p class="required">* Obavezna polja</p>
                                    <div class="buttons-set">
                                        <button id="send2" name="send" type="submit" class="button login"><span>Prijava</span></button>
                                        <a class="forgot-word" href="#">Forgot Your Password?</a>
                                    </div>
                                </form:form>

                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </section>
        <!-- Main Container End -->

        <!-- service -->
        <div class="our-features-box wow bounceInUp animated">
            <ul>
                <li>
                    <div class="feature-box">
                        <div class="icon-truck"></div>
                        <div class="content">FREE SHIPPING on order over $99</div>
                    </div>
                </li>
                <li>
                    <div class="feature-box">
                        <div class="icon-support"></div>
                        <div class="content">Need Help +1 800 123 1234</div>
                    </div>
                </li>
                <li>
                    <div class="feature-box">
                        <div class="icon-money"></div>
                        <div class="content">Money Back Guarantee</div>
                    </div>
                </li>
                <li class="last">
                    <div class="feature-box">
                        <div class="icon-return"></div>
                        <div class="content">30 days return Service</div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- end service -->

</t:wrapper>