<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
    <div class="header-banner">
        <div class="assetBlock">
            <p><a href="#">MENLOOK DAYS: UP TO <span>50%</span> OFF NEW SEASON ARRIVALS &gt; </a></p>
        </div>
        <div class="our-features-box">
            <div class="container">
                <ul>
                    <li>
                        <div class="feature-box">
                            <div class="icon-truck"></div>
                            <div class="content">FREE SHIPPING on order over $99</div>
                        </div>
                    </li>
                    <li>
                        <div class="feature-box">
                            <div class="icon-support"></div>
                            <div class="content">Need Help +1 800 123 1234</div>
                        </div>
                    </li>
                    <li>
                        <div class="feature-box">
                            <div class="icon-money"></div>
                            <div class="content">Money Back Guarantee</div>
                        </div>
                    </li>
                    <li class="last">
                        <div class="feature-box">
                            <div class="icon-return"></div>
                            <div class="content">30 days return Service</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Slider -->
    <div id="magik-slideshow" class="magik-slideshow">
        <div class="container">
            <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container' >
                <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                    <ul>
                        <li data-transition='random' data-slotamount='7' data-masterspeed='1000'><img src='../../resources/images/slide-img1.jpg'  data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="" />
                            <div class="info">
                                <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-x='0'  data-y='165'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;white-space:nowrap;'><span>New Season</span></div>
                                <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='0'  data-y='220'  data-endspeed='500'  data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;white-space:nowrap;'><span>Summer Sale</span></div>
                                <div class='tp-caption sfb  tp-resizeme ' data-x='0'  data-y='410'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'><a href='#' class="buy-btn">Shop Now</a></div>
                                <div    class='tp-caption Title sft  tp-resizeme ' data-x='0'  data-y='320'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'><h4 class="banner-text">In augue urna, nunc, tincidunt, augue, augue facilisis facilisis</h4></div>
                            </div>
                        </li>
                        <li data-transition='random' data-slotamount='7' data-masterspeed='1000'><img src='../../resources/images/slide-img2.jpg'  data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat'  alt=""/>
                            <div class="info">
                                <div class='tp-caption ExtraLargeTitle sft slide2  tp-resizeme ' data-x='45'  data-y='165'  data-endspeed='500'  data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;white-space:nowrap;padding-right:0px'>laptop Sale</div>
                                <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='45'  data-y='220'  data-endspeed='500'  data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;white-space:nowrap;'>Go Lightly</div>
                                <div    class='tp-caption sfb  tp-resizeme ' data-x='45'  data-y='400'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'><a href='#' class="buy-btn">Buy Now</a></div>
                                <div class='tp-caption Title sft  tp-resizeme ' data-x='45'  data-y='320'  data-endspeed='500'  data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'><h4 class="banner-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- end Slider -->

    <!-- banner -->
    <div class="top-banner-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow bounceup animated">
                    <div class="col"><a href="#"><img src="../../resources/images/block1.jpg" alt="offer banner3"></a></div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow bounceup animated">
                    <div class="col"><a href="#"><img src="../../resources/images/block1.jpg" alt="offer banner3"></a></div>
                </div>
                <div class="ccol-lg-4 col-md-4 col-sm-4 col-xs-12 wow bounceup animated">
                    <div class="col"><a href="#"><img src="../../resources/images/block1.jpg" alt="offer banner3"></a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end banner -->

    <!-- Featured Slider -->
    <section class="featured-pro container wow bounceInUp animated">
        <div class="slider-items-products">
            <div class="new_title center">
                <h2>ACCESSORIES</h2>
            </div>
            <div id="best-seller-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col4 products-grid">
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a>
                                    <div class="new-label new-top-left">new</div>
                                    <a href="quick_view.html" class="quickview-btn"><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a>
                                    <div class="sale-label">Sale</div>
                                    <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->

                    <!-- Item -->
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a> <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product</a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->

                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a>
                                    <div class="sale-label">Sale</div>
                                    <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a> <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->

                    <!-- Item -->
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a> <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->

                </div>
            </div>
        </div>
    </section>

    <!--Offer Start-->
    <div class="offer-slider wow animated parallax parallax-2">
        <div class="container">
            <ul class="bxslider">
                <li>
                    <h2>NEW ARRIVALS</h2>
                    <h1>Sale up to 30% off</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. </p>
                    <a class="shop-now" href="#">Shop now</a> </li>
                <li>
                    <h2>Hello hotness!</h2>
                    <h1>Summer collection</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. </p>
                    <a class="shop-now" href="#">View More</a> </li>
                <li>
                    <h2>New launch</h2>
                    <h1>Designer dresses on sale</h1>
                    <p>Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Integer sed arcu massa. </p>
                    <a class="shop-now" href="#">Learn More</a> </li>
            </ul>
        </div>
    </div>
    <!--Offer silder End-->

    <!-- Featured Slider -->
    <section class="featured-pro container wow bounceInUp animated">
        <div class="slider-items-products">
            <div class="new_title center">
                <h2>New Products</h2>
            </div>
            <div id="featured-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col4 products-grid">
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a>
                                    <div class="new-label new-top-left">new</div>
                                    <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a>
                                    <div class="sale-label">Sale</div>
                                    <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->

                    <!-- Item -->
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a> <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->

                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a>
                                    <div class="sale-label">Sale</div>
                                    <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a> <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a>
                                    <div class="sale-label">Sale</div>
                                    <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Item -->
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"> <img alt="Retis lapen casen" src="../../resources/products-images/product1.jpg"> </a> <a class="quickview-btn" ><span>Quick View</span></a> </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"> <a title="Retis lapen casen" href="product_detail.html"> Sample Product </a> </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating"></div>
                                                </div>
                                                <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                            </div>
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                                        </div>
                                        <div class="actions"><a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"></a>
                                            <div class="add_cart">
                                                <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                            </div>
                                            <a href="compare.html" class="link-compare" title="Add to Compare"></a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Item -->

                </div>
            </div>
        </div>
    </section>
    <!-- End Featured Slider -->

    <div class="brand-logo wow bounceInUp animated">
        <div class="container">
            <div class="new_title center">
                <h2>TOP BRANDS</h2>
            </div>
            <div class="slider-items-products">
                <div id="brand-logo-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col6">

                        <!-- Item -->
                        <div class="item"><a href="#"><img src="../../resources/images/b-logo3.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"><a href="#"><img src="../../resources/images/b-logo2.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"><a href="#"><img src="../../resources/images/b-logo1.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"><a href="#"><img src="../../resources/images/b-logo4.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"><a href="#"><img src="../../resources/images/b-logo5.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"><a href="#"><img src="../../resources/images/b-logo6.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"><a href="#"><img src="../../resources/images/b-logo1.png" alt="Image"></a> </div>
                        <!-- End Item -->

                        <!-- Item -->
                        <div class="item"><a href="#"><img src="../../resources/images/b-logo4.png" alt="Image"></a> </div>
                        <!-- End Item -->

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Latest Blog -->
    <section class="latest-blog wow bounceInUp animated">
        <div class="container">
            <div class="new_title center">
                <h2>Latest Blog</h2>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-4">
                    <div class="blog_inner">
                        <div class="blog-img"> <img src="../../resources/images/blog-img1.jpg" alt="Blog image">
                            <div class="mask"> <a class="info" href="blog_detail.html">Read More</a> </div>
                        </div>
                        <h3><a href="blog_detail.html">Pellentesque habitant morbi</a> </h3>
                        <div class="post-date"><i class="icon-calendar"></i> Apr 10, 2014</div>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce sit  ... </p>
                        <a class="readmore" href="blog_detail.html">Read More</a> </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-4">
                    <div class="blog_inner">
                        <div class="blog-img"> <img src="../../resources/images/blog-img1.jpg" alt="Blog image">
                            <div class="mask"> <a class="info" href="blog_detail.html">Read More</a> </div>
                        </div>
                        <h3><a href="blog_detail.html">Pellentesque habitant morbi</a> </h3>
                        <div class="post-date"><i class="icon-calendar"></i> Apr 10, 2014</div>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce sit  ... </p>
                        <a class="readmore" href="blog_detail.html">Read More</a> </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-4">
                    <div class="blog_inner">
                        <div class="blog-img"> <img src="../../resources/images/blog-img1.jpg" alt="Blog image">
                            <div class="mask"> <a class="info" href="blog_detail.html">Read More</a> </div>
                        </div>
                        <h3><a href="blog_detail.html">Pellentesque habitant morbi</a> </h3>
                        <div class="post-date"><i class="icon-calendar"></i> Apr 10, 2014</div>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce sit  ... </p>
                        <a class="readmore" href="blog_detail.html">Read More</a> </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Latest Blog -->
    <div class="header-banner mobile-show">
        <div class="our-features-box">
            <ul>
                <li>
                    <div class="feature-box">
                        <div class="icon-truck"></div>
                        <div class="content">FREE SHIPPING on order over $99</div>
                    </div>
                </li>
                <li>
                    <div class="feature-box">
                        <div class="icon-support"></div>
                        <div class="content">Need Help +1 800 123 1234</div>
                    </div>
                </li>
                <li>
                    <div class="feature-box">
                        <div class="icon-money"></div>
                        <div class="content">Money Back Guarantee</div>
                    </div>
                </li>
                <li class="last">
                    <div class="feature-box">
                        <div class="icon-return"></div>
                        <div class="content">30 days return Service</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</t:wrapper>