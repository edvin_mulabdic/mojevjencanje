<%@tag description="Wrapper for jsp" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tag" %>

<html>
<head>
    <title>Moje Vjenčanje</title>
    <link href="<c:url value="../../resources/css/bootstrap.min.css" />" rel="stylesheet">
    <!-- CSS Style -->
    <link href="<c:url value="../../resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="../../resources/css/animate.css"/>" rel="stylesheet">
    <link href="<c:url value="../../resources/css/revslider.css"/>" rel="stylesheet">
    <link href="<c:url value="../../resources/css/owl.carousel.css"/>" rel="stylesheet">
    <link href="<c:url value="../../resources/css/owl.theme.css"/>" rel="stylesheet">
    <link href="<c:url value="../../resources/css/jquery.bxslider.css"/>" rel="stylesheet">
    <link href="<c:url value="../../resources/css/jquery.mobile-menu.css"/>" rel="stylesheet">
    <link href="<c:url value="../../resources/css/style.css"/>" rel="stylesheet">
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200italic,300,300italic,400,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300,700,900' rel='stylesheet'>
</head>

<body>

<tag:header/>

<div id="body">
    <jsp:doBody/>
</div>

<tag:footer/>

<script src="../../resources/jquery-3.2.0/jquery-3.2.0.min.js"/>
<script src="../../resources/js/bootstrap.min.js"></script>
<%--<script src="../../resources/js/jquery.min.js"></script>--%>
<script src="../../resources/js/parallax.js"></script>
<script src="../../resources/js/revslider.js"></script>
<script src="../../resources/js/common.js"></script>
<script src="../../resources/js/jquery.bxslider.min.js"></script>
<script src="../../resources/js/owl.carousel.min.js"></script>
<script src="../../resources/js/jquery.mobile-menu.min.js"></script>
<script>
    jQuery(document).ready(function(){
        jQuery('#rev_slider_4').show().revolution({
            dottedOverlay: 'none',
            delay: 5000,
            startwidth: 1170,
            startheight: 600,

            hideThumbs: 200,
            thumbWidth: 200,
            thumbHeight: 50,
            thumbAmount: 2,

            navigationType: 'thumb',
            navigationArrows: 'solo',
            navigationStyle: 'round',

            touchenabled: 'on',
            onHoverStop: 'on',

            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,

            spinner: 'spinner0',
            keyboardNavigation: 'off',

            navigationHAlign: 'center',
            navigationVAlign: 'bottom',
            navigationHOffset: 0,
            navigationVOffset: 20,

            soloArrowLeftHalign: 'left',
            soloArrowLeftValign: 'center',
            soloArrowLeftHOffset: 20,
            soloArrowLeftVOffset: 0,

            soloArrowRightHalign: 'right',
            soloArrowRightValign: 'center',
            soloArrowRightHOffset: 20,
            soloArrowRightVOffset: 0,

            shadow: 0,
            fullWidth: 'on',
            fullScreen: 'off',

            stopLoop: 'off',
            stopAfterLoops: -1,
            stopAtSlide: -1,

            shuffle: 'off',

            autoHeight: 'off',
            forceFullWidth: 'on',
            fullScreenAlignForce: 'off',
            minFullScreenHeight: 0,
            hideNavDelayOnMobile: 1500,

            hideThumbsOnMobile: 'off',
            hideBulletsOnMobile: 'off',
            hideArrowsOnMobile: 'off',
            hideThumbsUnderResolution: 0,

            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            startWithSlide: 0,
            fullScreenOffsetContainer: ''
        });
    });
</script>
</body>
</html>